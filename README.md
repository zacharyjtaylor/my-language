# README #

This is the MY programming language, designed for math-oriented golfing (AKA, IO sucks).
It uses a stack-based postfix reversed order syntaxless approach (AKA, I don't know how to make anything else).
It is heavily influenced by Actually/Seriously/05AB1E (The biggest influences), Jelly, and APL.
~~Unlike these languages, MY (for now) has no (implemented) code page, but rather is implemented based upon bytes only (think GS2).~~

MY finally has a partial codepage implemented, 

MY stands for Michael-Yoda.

Michael was a failed language before MY, named after a short guy. :)

And Yoda, because the syntax (or lack thereof) resembles an OSV language. (Yoda)

MY will eventually have loops, conditionals, etc. The commands are/will be divided into Niladic, Monadic, Dyadic, and Triadic commands.

Requires: sympy and numpy.

*I am developing this language slowly.*