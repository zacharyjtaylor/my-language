#!encoding:utf-8

import math
import fractions
import cmath
import random
try:
	import numpy
	import numpy.linalg
	import sympy
except:
	print("Some functionality will not be available!")
import copy
import inspect
import sys
from functools import *
from itertools import permutations

try:
    new_input = raw_input
except:
    new_input = input

gstack = []
IO = 1 #index origin


arities = {}
for i in range(0xFF):
    if 0x00 <= i < 0x20:
        arities[chr(i)] = 0
    if 0x20 <= i < 0x80:
        arities[chr(i)] = 1 #Might break on functional commands.
    else:
        arities[chr(i)] = 2

reducer = lambda f,a:reduce(lambda *u:f(*u[::-1]),a[::-1])
def arity(s):
    M = 0
    for i in s:
        M = max(M,arities[i])
    return abs(M)
def arityF(f):
    try:
        return f.a
    except:
        return len(inspect.getargspec(f).args)

def scan(f,a):
    r = []
    if len(a) == 1: return a
    for i in range(len(a)):
        if i < 2:
            r = [f(a[0],a[1])]
        else:
            r.append(f(r[-1],a[i]))
    return [a[0]]+r

def lambdify(s,omega,alpha,stack=gstack):
    t = arity(s)
    def Inner(*a):
        b = list(copy.deepcopy(a))
        MainFunction(s,omega,alpha,b)
        return Pop(b)
    Inner.a = t
    return Inner

def stackify(f):
    def Inner(stack=gstack):
        ar = arityF(f)
        r = []
        for i in range(ar):
            r.append(Pop(stack))
        stack.append(f(*r))
    return Inner

def MapLeft(f):
    def Inner(x,*y):
        if type(x) == list:
            return list(map(lambda n: f(n,*y),x))
        else:
            return f(x,*y)
    Inner.a = arityF(f)
    return Inner

def MapEach(f):
    def Inner(*a):
        if all(type(i)==list for i in a):
            return list(map(f,*a))
        else:
            return f(*a)
    Inner.a = arityF(f)
    return Inner

def MapRight(f):
    def Inner(*a):
        if type(a[-1])==list:
            return list(map(lambda n:f(*(a[:-1]+(n,))),a[-1]))
        else:
            return f(*a)
    Inner.a = arityF(f)
    return Inner

def VecifyMonadic(f): #Extends monadic function to vectors
    def InnerFunction(arg): #Inner function to return
        if type(arg) == list:
            return list(map(VecifyMonadic(f),arg))
        else:
            return f(arg)
    return InnerFunction

def VecifyLeft(f):
    def Inner(x, *y):
        if type(x) == list:
            return list(map(lambda n: VecifyLeft(f)(n,*y),x))
        else:
            return f(x,*y)
    Inner.a = arityF(f)
    return Inner
  
def VecifyRight(f):
    def Inner(*a):
        if type(a[-1]) == list:
            return list(map(lambda n: VecifyRight(f)(*(a[:-1]+(n,))),a[-1]))
        else:
            return f(*a)
    Inner.a = arityF(f)
    return Inner
  
def VecifyDyadic(f):
    def Inner(x, y):
        if type(x) == list and type(y) == list:
            return list(map(VecifyDyadic(f),x,y))
        elif type(x) == list:
            return VecifyLeft(f)(x,y)
        elif type(y) == list:
            return VecifyRight(f)(x,y)
        else:
            return f(x,y)
    return Inner

def VecifyEach(f):
    if arityF(f) == 1:
        return VecifyMonadic(f)
    elif arityF(f) == 2:
        return VecifyDyadic(f)
    else:
        raise SyntaxError("sorry")

def scanr(f,a):
    r = []
    for i in range(len(a)):
        r.append(reducer(f,a[:i+1]))
    return r
def Pop(stack = gstack): #Utility function for popping value off stack, (including when stack is empty)
    if len(stack) == 0:
        return 0
    else:
        return stack.pop()
def StringOrd(arg): #Base function for code points
    if len(arg) == 1:
        return ord(arg)
    else:
        return list(map(ord,arg))
def isPrime(arg):
    res = 1
    for i in range(2,arg):
        if arg % i == 0:
            res = 0
    return int(int(arg) == arg and arg >= 2 and res) #Hack
def primeFunction(arg):
    tmp = 2
    cnt = 0
    while True:
        if isPrime(tmp):
            cnt += 1
        if cnt == arg:
            return tmp
        tmp += 1
def primesBelow(arg):
    res = []
    tmp = 1
    while True:
        if primeFunction(tmp) > arg:
            return res
        res.append(primeFunction(tmp))
        tmp += 1
def primeFactors(n):
    t = n
    c = 1
    r = [0]
    while t != 1:
        if len(r) != c:
            r.append(0)
        if t % primeFunction(c) == 0:
            r[-1] += 1
            t /= primeFunction(c)
        else:
            c += 1
    return r
def listFactors(n):
    r = []
    for i in range(1,n+1):
        if n % i == 0:
            r.append(i)
    return r
def iterable(argument): #Modified from Dennis's Jelly
    if type(argument) == list:
        return argument
    return [argument]
def flatten(argument): #Taken from Dennis's Jelly
    flat = []
    if type(argument) == list:
        for item in argument:
            flat += flatten(item)
    else:
        flat.append(argument)
    return flat
def increments(argument):
    res = []
    for i in range(len(argument)-1):
        res.append(VecifyDyadic(lambda w,a:w-a)(argument[i+1],argument[i]))
    return res
def selections(arr):
    res = []
    for i in range(len(arr)):
        res.extend(list(map(list,list(permutations(arr,i)))))
    return res
def basify(s):
    r = []
    for i in s:
        r.append("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".index(i))
    return r
def toBase36(s):
    if num == 0:
        return "0"
    S = []
    while num != 0:
        S.append("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[num % 36])
        num = int(num / 36)
    return "".join(S[::-1])
def rangeIO(n): #utility function for APL-style iota
    return list(map(lambda x:x+IO,n))
def gradeUp(a):
    return rangeIO(sorted(list(range(len(a))),key=lambda x:a[x]))
def gradeDown(a):
    return rangeIO(sorted(list(range(len(a))),key=lambda x:a[x],reverse=True))
def inverse(n):
    if type(n) == int or type(n) == float or type(n) == complex:
        return 1.0 / n
    else: #fractions.Fraction
        return 1 / n
def unique(a):
    if type(a) == str:
        return unique(list(a))
    elif type(a) == list:
        r = []
        for i in a:
            if not (i in r):
                r.append(i)
        return r
    else:
        return a
def truthyIndexes(a):
    r = []
    for i in range(len(a)):
        if a[i]:
            r.append(i + IO)
    return r
def reverseFn(f):
    def Inner(*u):
        return f(*u[::-1])
    Inner.a = arityF(f)
    return Inner
def trig(w,a):
    from sympy import sin,cos,tan,csc,sec,cot,sinh,cosh,tanh,csch,sech,coth
    return float([sin,cos,tan,csc,sec,cot,sinh,cosh,tanh,csch,sech,coth][w - IO](a))
def trigInv(w,a):
    from sympy import asin,acos,atan,acsc,asec,acot,asinh,acosh,atanh,asech,acoth#,acsch <- sympy doesn't have acsch for some dumb reason.
    acsch = lambda x : asinh(1.0/x)
    return float([asin,acos,atan,acsc,asec,acot,asinh,acosh,atanh,acsch,asech,acoth][w - IO](a))
def subvectors(arg):
    res = [[]]
    for i in range(len(arg)):
        for j in range(i,len(arg)):
            res.append(copy.deepcopy(arg[i:j+1]))
    return res
def nestIndex(w,a):
    if a == []:
        return w
    else:
        r = w[a[0] - IO]
        for i in a[1:]:
            r = r[i - IO]
        return r
def take(size,arg):
    if size < 0: return take(-size,arg[::-1])[::-1]
    if type(arg) == str:
        r = ''
        for i in range(size):
            if i >= len(arg):
                r += ' '
            else:
                r += arg[i]
        return r
    elif type(arg) == list:
        r = []
        for i in range(size):
            if i >= len(arg):
                r.append(0)
            else:
                r.append(arg[i])
        return r
    else:
        return take(size,[arg])
def drop(size,arg):
    if size >= 0:
        return arg[size:]
    else:
        return arg[:size]
def sgn(w):
    if w > 0:
        return 1
    elif w == 0:
        return 0
    elif w < 0:
        return -1
    else:
        return None
def round_(num):
    num = float(num)
    if num % 2 == 0.5:
        return math.trunc(num) + sgn(num)
    else:
        return round(num)

floor = lambda x : int(math.floor(float(x) + 0.1*(float(x)%1==0.5)))
ceil = lambda x : -floor(-x)

def repeat(array,num):
    if num < 0:
        return repeat(array[::-1],-num)
    elif num == 0:
        return []
    elif num == int(num):
        return array * num
    elif num > 1:
        return array + repeat(array,num-1)
    else:
        return array[:round_(num * len(array))]
def find(w,a):
    res = []
    for i in range(len(a)):
        if a[i] == w:
            res.append(i + IO)
    return res
def mold(content, shape):
    for index in range(len(shape)):
        if type(shape[index]) == list:
            mold(content, shape[index])
        else:
            item = content.pop(0)
            shape[index] = item
            content.append(item)
    return shape
def matmul(left,right):
    return numpy.dot(
            numpy.matrix(left),
            numpy.matrix(right)
    ).tolist()

#Niladic functions

def Push0(w,a,stack=gstack):
    stack.append(0x0)
def Push1(w,a,stack=gstack):
    stack.append(0x1)
def Push2(w,a,stack=gstack):
    stack.append(0x2)
def Push3(w,a,stack=gstack):
    stack.append(0x3)
def Push4(w,a,stack=gstack):
    stack.append(0x4)
def Push5(w,a,stack=gstack):
    stack.append(0x5)
def Push6(w,a,stack=gstack):
    stack.append(0x6)
def Push7(w,a,stack=gstack):
    stack.append(0x7)
def Push8(w,a,stack=gstack):
    stack.append(0x8)
def Push9(w,a,stack=gstack):
    stack.append(0x9)
def PushA(w,a,stack=gstack):
    stack.append(0xA)
def PushB(w,a,stack=gstack):
    stack.append(0xB)
def PushC(w,a,stack=gstack):
    stack.append(0xC)
def PushD(w,a,stack=gstack):
    stack.append(0xD)
def PushE(w,a,stack=gstack):
    stack.append(0xE)
def PushF(w,a,stack=gstack):
    stack.append(0xF)
def Omega(w,a,stack=gstack):
    stack.append(w)
def Alpha(w,a,stack=gstack):
    stack.append(a)
def OmegaLambda(w,a,stack=gstack):
    stack.append(lambda *args:args[0])
def AlphaLambda(w,a,stack=gstack):
    stack.append(lambda *args:args[1])
def EmptyString(w,a,stack=gstack):
    stack.append("")
def EmptyArray(w,a,stack=gstack):
    stack.append([])
def ImaginaryUnit(w,a,stack=gstack):
    stack.append(1j)
def PushExp1(w,a,stack=gstack): #I already used PushE
    stack.append(math.e)
def PushPi(w,a,stack=gstack):
    stack.append(math.pi)
def InputBoolean(w,a,stack=gstack):
    stack.append(new_input() != "")
def InputInteger(w,a,stack=gstack):
    stack.append(int(new_input()))
def InputRational(w,a,stack=gstack):
    i = new_input()
    ns,ds = i.split("/")
    n,d = int(ns),int(ds)
    stack.append(fractions.Fraction(n,d))
def InputFloat(w,a,stack=gstack):
    stack.append(float(new_input()))
def PushNewline(w,a,stack=gstack):
    stack.append("\n")
def InputRaw(w,a,stack=gstack):
    stack.append(new_input())
def InputEval(w,a,stack=gstack):
    stack.append(eval(new_input()))

#Monadic functions
def Decrement(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:x - 1)(w))
def Increment(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:x + 1)(w))
def Negative(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x: -x)(w))
def AbsoluteValue(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(abs)(w))
def DecimalShift(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:10*x)(w))
def HexShift(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:16*x)(w))
def Output(w_,a,stack=gstack):
    w = Pop(stack)
    sys.stdout.write(str(w))
def OutputLn(w_,a,stack=gstack):
    w = Pop(stack)
    print(w)
def ToChar(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(chr)(w))
def CodePoint(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(StringOrd)(w))
def SignFunction(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(sgn)(w))
def Factorial(w_,a,stack=gstack):
    f = lambda x : 1 if x == 0 else x*f(x-1)
    w = Pop(stack)
    stack.append(VecifyMonadic(f)(w))
def Exponential(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : math.e ** x)(w))
def Log2(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : math.log(x,2))(w))
def Ln(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : math.log(x,math.e))(w))
def Log10(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : math.log(x,10))(w))
def SquareRoot(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(math.sqrt)(w))
def ComplexConjugate(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : x.conjugate())(w))
def ComplexPhase(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(cmath.phase)(w))
def RealPart(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : x.real)(w))
def ImagPart(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : x.imag)(w))
def IsPrime(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(isPrime)(w))
def PrimeFunction(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(primeFunction)(w))
def PrimesBelow(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(primesBelow)(w))
def PrimeList(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x : list(map(primeFunction,range(1,x+1))))(w))
def PrimeFactors(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(primeFactors)(w))
def ListFactors(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(listFactors)(w))
def Boolean(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(1-int(w == 0 or w == '' or w == []))
def NotBoolean(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(int(w == 0 or w == '' or w == []))
def Floor(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(floor)(w)) #I hate you python3, why do you use banker's rounding?!
def Round(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(round_)(w))
def Ceiling(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(ceil)(w))
def ToFraction(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(fractions.Fraction)(w))
def Float(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(float)(w))
def ToString(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(str(w))
def ToRepr(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(repr(w))
def FromString(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(eval)(w))
def RandomChoice(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(random.choice(w))
def RandomIota(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:random.randint(1,x))(w))
def Length(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(len(w))
def SetIO(w_,a,stack=gstack):
    global IO
    w = Pop(stack)
    IO = w
def Iota(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:list(range(1,x+1)))(w)) #Python3 range is a class
def Range(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:list(range(x)))(w)) #Same here.
def RangeIndex(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(list(range(IO,IO+len(w))))
def Reverse(w_,a,stack=gstack):
    w = Pop(stack)
    if type(w) == str or type(w) == list:
        stack.append(w[::-1])
    else:
        S = str(w)[::-1]
        while len(S) > 1 and S[0]=='0':
            S = S[1:]
        stack.append(eval(S))
def Wrap(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append([w])
def Tighten(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(sum(map(iterable, iterable(w)), [])) #Taken from Dennis's Jelly
def Flatten(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(flatten(w))
def Transpose(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(list(map(list,zip(*w))))
def Determinant(w_,a,stack=gstack):
    w = Pop(stack)
    wa = numpy.matrix(w)
    stack.append(numpy.linalg.det(wa).tolist())
def Inverse(w_,a,stack=gstack):
    w = Pop(stack)
    wa = numpy.matrix(w)
    stack.append((w**-1).tolist())
def Sum(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(reduce(VecifyDyadic(lambda x=0,y=0:x+y),w,0))
def Product(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(reduce(VecifyDyadic(lambda x=1,y=1:x*y),w,1))
def Increments(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(increments(w))
def EmptyJoin(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(''.join(w))
def EmptySplit(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(list(w))
def SpaceJoin(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(' '.join(w))
def SpaceSplit(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(w.split(' '))
def LineJoin(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append('\n'.join(w))
def LineSplit(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(w.split(w))
def Subvectors(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(subvectors(w))
def Permutations(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(list(map(list,list(permutations(w)))))
def Selections(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(selections(w))
def Basify(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(basify)(w))
def Stringify(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda n : "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[n])(w))
def ToBinary(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda x:list(map(int,bin(x)[2:])))(w))
def FromBinary(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(int(''.join(map(str,w)),2))
def ToHex(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(lambda n:hex(n).upper()[2:])(w))
def FromHex(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(int(w,16))
def ToBase36(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(toBase36)(w))
def FromBase36(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(int(w,36))
def GradeUp(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(gradeUp(w))
def SortUp(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(sorted(w))
def GradeDown(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(gradeDown(w))
def SortDown(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(sorted(w,reverse=True))
def Inverse(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyMonadic(inverse)(w))
def Minimum(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(min(w))
def Maximum(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(max(w))
def Unique(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(unique(w))
def TruthyIndexes(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(truthyIndexes(w))
def Lambdify(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambdify(w,w_,a))
def Apply(w_,a,stack=gstack):
    w = Pop(stack)
    stackify(w)(stack)
def Filter(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:list(filter(w,n)))
def FilterNot(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:list(filter(lambda x:int(not w(x)),n)))
def ReduceLeft(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:reduce(w,n))
def ReduceRight(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:reducer(w,n))
def ScanLeft(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:scan(w,n))
def ScanRight(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:scanr(w,n))
def ML(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(MapLeft(w))
def ME(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(MapEach(w))
def MR(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(MapRight(w))
def VL(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyLeft(w))
def VE(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyEach(w))
def VR(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(VecifyRight(w))
def Commute(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(lambda n:w(*([n]*arityF(w))))
def ReverseFn(w_,a,stack=gstack):
    w = Pop(stack)
    stack.append(reverseFn(w))
    
#Dyadic commands
def Plus(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a+b)(w,a))
def Minus(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a-b)(w,a))
def Times(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a*b)(w,a))
def RationalDivide(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:fractions.Fraction(a,b))(w,a))
def FloatDivide(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:float(a)/b)(w,a))
def IntegerDivide(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a/b))(w,a))
def Mod(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a%b)(w,a))
def Power(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a**b)(w,a))
def DecimalConcat(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a*10+b)(w,a))
def HexConcat(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:a*16+b)(w,a))
def DecimalPoint(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:float(str(a) + '.' + str(b)))(w,a))
def Trig(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(trig)(w,a))
def TrigInv(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(trigInv)(w,a))
def LogBase(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(math.log)(w,a))
def Match(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(int(w == a))
def NotMatch(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(int(w != a))
def Equal(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a==b))(w,a))
def NotEqual(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a!=b))(w,a))
def Less(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a<b))(w,a))
def LessEqual(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a<=b))(w,a))
def GreaterEqual(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a>=b))(w,a))
def Greater(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:int(a>b))(w,a))
def Pair(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append([w,a])
def CatEnclose(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    if type(w) != list and type(w) != str:
        w = [w]
    stack.append(w + [a])
def Cat(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    if type(w) != list and type(w) != str:
        w = [w]
    if type(a) != list and type(a) != str:
        a = [a]
    stack.append(w + a)
def IndexRecurse(w_,a_,stack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyMonadic(lambda x:w[x-IO])(a))
def NestIndex(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(nestIndex(w,a))
def LCM(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(sympy.lcm)(w,a))
def GCD(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(sympy.gcd)(w,a))
def ExclusiveRange(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:list(range(a,b)))(w,a))
def RandomInteger(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(random.randrange)(w,a))
def RandomFloat(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda a,b:random.random()*(b-a)+a)(w,a))
def MinimumArg(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda x,y: min(x,y))(w,a))
def MaximumArg(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyDyadic(lambda x,y: max(x,y))(w,a))
def Take(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyLeft(take)(w,a))
def Drop(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyLeft(drop)(w,a))
def Repeat(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(VecifyRight(repeat)(w,a))
def Find(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(find(w,a))
def Mold(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(mold(w,a))
def MatMul(w_,a_,stack=gstack):
    w = Pop(stack)
    a = Pop(stack)
    stack.append(matmul(w,a))

def MainFunction(code,w=None,a=None,stack=gstack):
    for i in code:
        if i == '\x00':
            Push0(w,a,stack)
        elif i == '\x01':
            Push1(w,a,stack)
        elif i == '\x02':
            Push2(w,a,stack)
        elif i == '\x03':
            Push3(w,a,stack)
        elif i == '\x04':
            Push4(w,a,stack)
        elif i == '\x05':
            Push5(w,a,stack)
        elif i == '\x06':
            Push6(w,a,stack)
        elif i == '\x07':
            Push7(w,a,stack)
        elif i == '\x08':
            Push8(w,a,stack)
        elif i == '\x09':
            Push9(w,a,stack)
        elif i == '\x0A':
            PushA(w,a,stack)
        elif i == '\x0B':
            PushB(w,a,stack)
        elif i == '\x0C':
            PushC(w,a,stack)
        elif i == '\x0D':
            PushD(w,a,stack)
        elif i == '\x0E':
            PushE(w,a,stack)
        elif i == '\x0F':
            PushF(w,a,stack)
        elif i == '\x10':
            Omega(w,a,stack)
        elif i == '\x11':
            Alpha(w,a,stack)
        elif i == '\x12':
            OmegaLambda(w,a,stack)
        elif i == '\x13':
            AlphaLambda(w,a,stack)
        elif i == '\x14':
            EmptyString(w,a,stack)
        elif i == '\x15':
            EmptyArray(w,a,stack)
        elif i == '\x16':
            ImaginaryUnit(w,a,stack)
        elif i == '\x17':
            PushExp1(w,a,stack)
        elif i == '\x18':
            PushPi(w,a,stack)
        elif i == '\x19':
            InputBoolean(w,a,stack)
        elif i == '\x1A':
            InputInteger(w,a,stack)
        elif i == '\x1B':
            InputRational(w,a,stack)
        elif i == '\x1C':
            InputFloat(w,a,stack)
        elif i == '\x1D':
            PushNewline(w,a,stack)
        elif i == '\x1E':
            InputRaw(w,a,stack)
        elif i == '\x1F':
            InputEval(w,a,stack)
        elif i == '\x20':
            Decrement(w,a,stack)
        elif i == '\x21':
            Increment(w,a,stack)
        elif i == '\x22':
            Negative(w,a,stack)
        elif i == '\x23':
            AbsoluteValue(w,a,stack)
        elif i == '\x24':
            DecimalShift(w,a,stack)
        elif i == '\x25':
            HexShift(w,a,stack)
        elif i == '\x26':
            Output(w,a,stack)
        elif i == '\x27':
            OutputLn(w,a,stack)
        elif i == '\x28':
            ToChar(w,a,stack)
        elif i == '\x29':
            CodePoint(w,a,stack)
        elif i == '\x2A':
            SignFunction(w,a,stack)
        elif i == '\x2B':
            Factorial(w,a,stack)
        elif i == '\x2C':
            Exponential(w,a,stack)
        elif i == '\x2D':
            Log2(w,a,stack)
        elif i == '\x2E':
            Ln(w,a,stack)
        elif i == '\x2F':
            Log10(w,a,stack)
        elif i == '\x30':
            SquareRoot(w,a,stack)
        elif i == '\x31':
            ComplexConjugate(w,a,stack)
        elif i == '\x32':
            ComplexPhase(w,a,stack)
        elif i == '\x33':
            RealPart(w,a,stack)
        elif i == '\x34':
            ImagPart(w,a,stack)
        elif i == '\x35':
            IsPrime(w,a,stack)
        elif i == '\x36':
            PrimeFunction(w,a,stack)
        elif i == '\x37':
            PrimesBelow(w,a,stack)
        elif i == '\x38':
            PrimeList(w,a,stack)
        elif i == '\x39':
            PrimeFactors(w,a,stack)
        elif i == '\x3A':
            ListFactors(w,a,stack)
        elif i == '\x3B':
            Boolean(w,a,stack)
        elif i == '\x3C':
            NotBoolean(w,a,stack)
        elif i == '\x3D':
            Floor(w,a,stack)
        elif i == '\x3E':
            Round(w,a,stack)
        elif i == '\x3F':
            Ceiling(w,a,stack)
        elif i == '\x40':
            ToFraction(w,a,stack)
        elif i == '\x41':
            Float(w,a,stack)
        elif i == '\x42':
            ToString(w,a,stack)
        elif i == '\x43':
            ToRepr(w,a,stack)
        elif i == '\x44':
            FromString(w,a,stack)
        elif i == '\x45':
            RandomChoice(w,a,stack)
        elif i == '\x46':
            RandomIota(w,a,stack)
        elif i == '\x47':
            Length(w,a,stack)
        elif i == '\x48':
            SetIO(w,a,stack)
        elif i == '\x49':
            Iota(w,a,stack)
        elif i == '\x4A':
            Range(w,a,stack)
        elif i == '\x4B':
            RangeIndex(w,a,stack)
        elif i == '\x4C':
            Reverse(w,a,stack)
        elif i == '\x4D':
            Wrap(w,a,stack)
        elif i == '\x4E':
            Tighten(w,a,stack)
        elif i == '\x4F':
            Flatten(w,a,stack)
        elif i == '\x50':
            Transpose(w,a,stack)
        elif i == '\x51':
            Determinant(w,a,stack)
        elif i == '\x52':
            Inverse(w,a,stack)
        elif i == '\x53':
            Sum(w,a,stack)
        elif i == '\x54':
            Product(w,a,stack)
        elif i == '\x55':
            Increments(w,a,stack)
        elif i == '\x56':
            EmptyJoin(w,a,stack)
        elif i == '\x57':
            EmptySplit(w,a,stack)
        elif i == '\x58':
            SpaceJoin(w,a,stack)
        elif i == '\x59':
            SpaceSplit(w,a,stack)
        elif i == '\x5A':
            LineJoin(w,a,stack)
        elif i == '\x5B':
            LineSplit(w,a,stack)
        elif i == '\x5C':
            Subvectors(w,a,stack)
        elif i == '\x5D':
            Permutations(w,a,stack)
        elif i == '\x5E':
            Selections(w,a,stack)
        elif i == '\x5F':
            Basify(w,a,stack)
        elif i == '\x60':
            Stringify(w,a,stack)
        elif i == '\x61':
            ToBinary(w,a,stack)
        elif i == '\x62':
            FromBinary(w,a,stack)
        elif i == '\x63':
            ToHex(w,a,stack)
        elif i == '\x64':
            FromHex(w,a,stack)
        elif i == '\x65':
            ToBase36(w,a,stack)
        elif i == '\x66':
            FromBase36(w,a,stack)
        elif i == '\x67':
            GradeUp(w,a,stack)
        elif i == '\x68':
            SortUp(w,a,stack)
        elif i == '\x69':
            GradeDown(w,a,stack)
        elif i == '\x6A':
            SortDown(w,a,stack)
        elif i == '\x6B':
            Inverse(w,a,stack)
        elif i == '\x6C':
            Minimum(w,a,stack)
        elif i == '\x6D':
            Maximum(w,a,stack)
        elif i == '\x6E':
            Unique(w,a,stack)
        elif i == '\x6F':
            TruthyIndexes(w,a,stack)
        elif i == '\x70':
            Lambdify(w,a,stack)
        elif i == '\x71':
            Apply(w,a,stack)
        elif i == '\x72':
            Filter(w,a,stack)
        elif i == '\x73':
            FilterNot(w,a,stack)
        elif i == '\x74':
            ReduceLeft(w,a,stack)
        elif i == '\x75':
            ReduceRight(w,a,stack)
        elif i == '\x76':
            ScanLeft(w,a,stack)
        elif i == '\x77':
            ScanRight(w,a,stack)
        elif i == '\x78':
            ML(w,a,stack)
        elif i == '\x79':
            ME(w,a,stack)
        elif i == '\x7A':
            MR(w,a,stack)
        elif i == '\x7B':
            VL(w,a,stack)
        elif i == '\x7C':
            VE(w,a,stack)
        elif i == '\x7D':
            VR(w,a,stack)
        elif i == '\x7E':
            Commute(w,a,stack)
        elif i == '\x7F':
            ReverseFn(w,a,stack)
        elif i == '\x80':
            Plus(w,a,stack)
        elif i == '\x81':
            Minus(w,a,stack)
        elif i == '\x82':
            Times(w,a,stack)
        elif i == '\x83':
            RationalDivide(w,a,stack)
        elif i == '\x84':
            FloatDivide(w,a,stack)
        elif i == '\x85':
            IntegerDivide(w,a,stack)
        elif i == '\x86':
            Mod(w,a,stack)
        elif i == '\x87':
            Power(w,a,stack)
        elif i == '\x88':
            DecimalConcat(w,a,stack)
        elif i == '\x89':
            HexConcat(w,a,stack)
        elif i == '\x8A':
            DecimalPoint(w,a,stack)
        elif i == '\x8B':
            Trig(w,a,stack)
        elif i == '\x8C':
            TrigInv(w,a,stack)
        elif i == '\x8D':
            LogBase(w,a,stack)
        elif i == '\x8E':
            Match(w,a,stack)
        elif i == '\x8F':
            NotMatch(w,a,stack)
        elif i == '\x90':
            Equal(w,a,stack)
        elif i == '\x91':
            NotEqual(w,a,stack)
        elif i == '\x92':
            Less(w,a,stack)
        elif i == '\x93':
            LessEqual(w,a,stack)
        elif i == '\x94':
            GreaterEqual(w,a,stack)
        elif i == '\x95':
            Greater(w,a,stack)
        elif i == '\x96':
            Pair(w,a,stack)
        elif i == '\x97':
            CatEnclose(w,a,stack)
        elif i == '\x98':
            Cat(w,a,stack)
        elif i == '\x99':
            IndexRecurse(w,a,stack)
        elif i == '\x9A':
            NestIndex(w,a,stack)
        elif i == '\x9B':
            LCM(w,a,stack)
        elif i == '\x9C':
            GCD(w,a,stack)
        elif i == '\x9D':
            ExclusiveRange(w,a,stack)
        elif i == '\x9E':
            RandomInteger(w,a,stack)
        elif i == '\x9F':
            RandomFloat(w,a,stack)
        elif i == '\xA0':
            MinimumArg(w,a,stack)
        elif i == '\xA1':
            MaximumArg(w,a,stack)
        elif i == '\xA2':
            Take(w,a,stack)
        elif i == '\xA3':
            Drop(w,a,stack)
        elif i == '\xA4':
            Repeat(w,a,stack)
        elif i == '\xA5':
            Find(w,a,stack)
        elif i == '\xA6':
            Mold(w,a,stack)
        elif i == '\xA7':
            MatMul(w,a,stack)
        else:
            print("Hello, World!")

cp = u"0123456789ABCDEF"
cp += u"ωα⊢⊣ɛ⍬ⅈⅇπ𝕓𝕫𝕢𝕣␤⍞⎕"
cp += u"’‘_|ȦĠ←↵'⍘±!*₂ₑₓ"
cp += u"√¯∠ℜℑṖṄṘṀĖḊ𝔹¬⌊[⌈"
cp += u"ℚℝ\"`´⍰?#⌶iίι⌽WYḟ"
cp += u"⍉∥¹ΣΠΔέηυύnń𝟚℘𝒫P"
cp += u"pRrHhTt⍋⍏⍒⍖⍁⍗⍐ū⍸"
cp += u"ƒ(fɫ⇤⇥⇠⇢⇷⇹⇸⇺⇼⇻ï⍨"
cp += u"+-×÷/\%^áǵ.oōₙ≡≢"
cp += u"=≠<≤≥> ,;@à∧∨…xX"
cp += u"«»↑↓⌿⍷m⊗⊤⊥⊖csj⊆g"

def RunCode(arg,*stuff): #Run with the codepage
    MainFunction(''.join(chr(cp.index(i))for i in arg),*stuff)

#To run, code with arguments w [first argument] and a [second argument]:
#Run python REPL with this file in the directory, then type:
#import main
#RunCode(code,...)

